package com.equipo2.onboardingdigital.controllers;


import com.equipo2.onboardingdigital.services.ProductServiceResponse;
import com.equipo2.onboardingdigital.models.ProductModel;
import com.equipo2.onboardingdigital.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/onboarding/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");
        System.out.println("Consulta de todos los productos a ofrecer");

        return new ResponseEntity<>(
        this.productService.findAll()
        , HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
            result.isPresent() ? result.get() : "SE ENCUENTRA PRODUCTO",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductServiceResponse> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduc");
        System.out.println("La id del nuevo producto es: " + product.getId());
        System.out.println("El Nombre del producto a crear es: " + product.getName());
        System.out.println("La Descripción de producto a crear es: " + product.getDescription());
        System.out.println("El Umbral Máximo del producto a crear es:" + product.getAcceptanceMax());
        System.out.println("El Umbral Mínimo de producto a crear es: " + product.getAcceptanceMin());
        System.out.println("La Familia al que pertenece el producto a crear es: " + product.getFamily());
        System.out.println("El límite de productos a contratar es: " + product.getHiringLimit());
        System.out.println("El precio de productos a contratar es: " + product.getPrice());

        ProductServiceResponse response = this.productService.add(product);

        return new ResponseEntity<>(
                response,response.getResponseHttpStatus()
        );
    }
}
