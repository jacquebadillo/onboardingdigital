package com.equipo2.onboardingdigital.controllers;

import com.equipo2.onboardingdigital.models.CustomersModel;
import com.equipo2.onboardingdigital.services.CustomerService;
import com.equipo2.onboardingdigital.services.CustomerServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/onboarding/v2")
public class CustomersController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")
    public ResponseEntity<List<CustomersModel>> getCustomers() {
        System.out.println(" ::getCustomers:: ");

        return new ResponseEntity<>(
                this.customerService.findAll()
                , HttpStatus.OK
        );
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable String id) {
        System.out.println(" ::getCustomerstById:: " + id);

        Optional<CustomersModel> result = this.customerService.getById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "No existe el cliente",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/customers")
    public ResponseEntity<CustomerServiceResponse> addCustomer(@RequestBody CustomersModel customer){
        System.out.println(" ::addCustomer:: " + customer.getId());

        return  new ResponseEntity<>(
                this.customerService.add(customer)
                , HttpStatus.CREATED
        );
    }
}
