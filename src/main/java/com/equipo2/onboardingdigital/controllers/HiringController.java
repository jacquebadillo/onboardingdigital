package com.equipo2.onboardingdigital.controllers;

import com.equipo2.onboardingdigital.models.*;
import com.equipo2.onboardingdigital.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/onboarding/v2")
public class HiringController {

    @Autowired
    HiringService hiringService;

    @GetMapping("/hirings/{family}/{idEnterprise}")
    public ResponseEntity<?> getHiringByFamilyandEnterpriseId(@PathVariable String family, @PathVariable String idEnterprise){

        Optional<List<HiringModel>> result = this.hiringService.getHiringsByIdAndFamily(idEnterprise, family);

        return new ResponseEntity<>(
                result.isPresent() ? result : "No se encontro valor",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NO_CONTENT
        );
    }

    @GetMapping("/hirings")
    public ResponseEntity<?> getHiring(){

        System.out.println("getHiringAll");
        List<HiringModel> result = this.hiringService.getHirings();

        return new ResponseEntity<>(
                null != result ? result : "No se encontraron resultados",
                null != result ? HttpStatus.OK : HttpStatus.NO_CONTENT
        );
    }

    @GetMapping("/hirings/{id}")
    public ResponseEntity<?> getHiringById(@PathVariable String id){

        System.out.println("getHiringById ");
        System.out.println("getHiringById Se busca por id " + id);
        Optional<HiringModel> result = this.hiringService.getHinringsById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "No se encontraron resultados",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NO_CONTENT
        );
    }

    @PostMapping("/hirings")
    public ResponseEntity<HiringServiceResponse> addHiring(@RequestBody HiringModel hiringModel){

        System.out.println("addHiring");
        System.out.println("addHiring se agregara id " + hiringModel.getId());
        System.out.println("addHiring se agregara idEmpresa " + hiringModel.getIdEnterprise());
        System.out.println("addHiring se agregara idProduct " + hiringModel.getIdProduct());
        System.out.println("addHiring se agregara hiringDate " + hiringModel.getHiringDate());
        System.out.println("addHiring se agregara family " + hiringModel.getFamily());

        HiringServiceResponse result = this.hiringService.addHiring(hiringModel);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }



}
