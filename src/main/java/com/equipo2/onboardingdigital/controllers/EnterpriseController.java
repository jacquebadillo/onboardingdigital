package com.equipo2.onboardingdigital.controllers;

import com.equipo2.onboardingdigital.models.EnterpriseModel;
import com.equipo2.onboardingdigital.services.EnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/onboarding/v2")
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;

    @GetMapping("/enterprises")
    public ResponseEntity<List<EnterpriseModel>> getEnterprises() {
        final List<EnterpriseModel> response = enterpriseService.findAll();
        return new ResponseEntity<>(response, (response != null && !response.isEmpty()) ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }

    @GetMapping("/enterprises/{id-enterprise}")
    public ResponseEntity<EnterpriseModel> getEnterprise(@PathVariable("id-enterprise") String idEnterprise) {
        final Optional<EnterpriseModel> response = enterpriseService.findById(idEnterprise);
        return new ResponseEntity<>(response.orElse(null), response.orElse(null) != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/enterprises")
    public ResponseEntity<EnterpriseModel> createEnterprise(@RequestBody EnterpriseModel enterpriseModel) {
        return new ResponseEntity<>(enterpriseService.save(enterpriseModel), HttpStatus.CREATED);
    }

}
