package com.equipo2.onboardingdigital.repositories;

import com.equipo2.onboardingdigital.models.CustomersModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<CustomersModel, String> {

}
