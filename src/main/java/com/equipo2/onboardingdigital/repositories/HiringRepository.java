package com.equipo2.onboardingdigital.repositories;

import com.equipo2.onboardingdigital.models.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HiringRepository extends MongoRepository<HiringModel, String> {

    @Query(value = "{ 'family' :  ?0, 'idEnterprise' : ?1  }")
    Optional<List<HiringModel>> findByEnterpriseIdAndFamilyProduct(String family, String idEnterprise);

    @Query(value = "{'idEnterprise' : ?0  }")
    Optional<List<HiringModel>> findByEnterpriseId(String idEnterprise);
}
