package com.equipo2.onboardingdigital.repositories;

import com.equipo2.onboardingdigital.models.EnterpriseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnterpriseRepository extends MongoRepository<EnterpriseModel, String> {

}