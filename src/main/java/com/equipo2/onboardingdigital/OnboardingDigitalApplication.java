package com.equipo2.onboardingdigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class OnboardingDigitalApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(OnboardingDigitalApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(OnboardingDigitalApplication.class, args);
	}

}
