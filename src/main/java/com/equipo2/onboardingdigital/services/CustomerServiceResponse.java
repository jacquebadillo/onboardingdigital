package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.CustomersModel;
import org.springframework.http.HttpStatus;

public class CustomerServiceResponse {
    private String msg;
    private CustomersModel customer;
    private HttpStatus responseHttpStatusCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CustomersModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomersModel customer) {
        this.customer = customer;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
