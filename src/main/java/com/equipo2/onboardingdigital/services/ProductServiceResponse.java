package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.ProductModel;
import org.springframework.http.HttpStatus;

public class ProductServiceResponse {

    private String msg;
    private ProductModel productModel;
    private HttpStatus responseHttpStatus;

    public ProductServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProductModel getProductModel() {
        return productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public HttpStatus getResponseHttpStatus() {
        return responseHttpStatus;
    }

    public void setResponseHttpStatus(HttpStatus responseHttpStatus) {
        this.responseHttpStatus = responseHttpStatus;
    }
}
