package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.HiringModel;
import org.springframework.http.HttpStatus;

public class HiringServiceResponse {

    private String msg;
    private HiringModel hiring;
    private HttpStatus responseHttpStatusCode;

    public HiringServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HiringModel getHiring() {
        return hiring;
    }

    public void setHiring(HiringModel hiring) {
        this.hiring = hiring;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}