package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.*;
import com.equipo2.onboardingdigital.repositories.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HiringService {

    @Autowired
    HiringRepository hiringRepository;

    @Autowired
    EnterpriseService enterpriseService;

    @Autowired
    ProductService productService;


    public List<HiringModel> getHirings() {

        System.out.println("HiringService getHirings");

       return this.hiringRepository.findAll();
    }

    public Optional<HiringModel> getHinringsById(String id) {
        System.out.println("HiringService getHiringsById");

        return this.hiringRepository.findById(id);
    }

    public Optional<List<HiringModel>> getHiringsByIdAndFamily(String idEnterprise, String family){
        System.out.println("Hiring Service getHiringByIdAndFamily");
        System.out.println("Hiring Service getHiringByIdAndFamily family: " + family);
        System.out.println("Hiring Service getHiringByIdAndFamily idEnterprise: " + idEnterprise);

        return this.hiringRepository.findByEnterpriseIdAndFamilyProduct(family, idEnterprise);
    }

    public HiringServiceResponse addHiring(HiringModel hiring) {

        System.out.println("addHiring");
        HiringServiceResponse result = new HiringServiceResponse();
        float amount = 0;

        result.setHiring(hiring);

        //Consulta la existencia de la empresa
        Optional<EnterpriseModel> enterpriseResult = this.enterpriseService.findById(hiring.getIdEnterprise());

        //Consulta la existencia de los productos
        Optional<ProductModel> productResult = this.productService.findById(hiring.getIdProduct());

        //Consulta el numero de contrataciones por familia
        Optional<List<HiringModel>> hiringResult = this.hiringRepository.findByEnterpriseIdAndFamilyProduct(hiring.getFamily(), hiring.getIdEnterprise());

        //Consulta el numero de contrataciones por empresa
        Optional<List<HiringModel>> hiringAllResult = this.hiringRepository.findByEnterpriseId(hiring.getIdEnterprise());

        //Consulta folio de contratacion
        Optional<HiringModel> hiringPointResult = this.hiringRepository.findById(hiring.getId());

        if(!enterpriseResult.isPresent()){
            System.out.println("La empresa no existe");
            result.setMsg("La empresa no existe");
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);

            return result;
        }

        if(!productResult.isPresent()){
            System.out.println("El producto a contratar no existe");
            result.setMsg("El producto a contratar no existe");
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);

            return result;
        }

        if(hiringPointResult.isPresent()){
            System.out.println("El id de contratacion ya existe");
            result.setMsg("El id de contratacion ya existe");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

           return result;
        }

        for(HiringModel hM : hiringAllResult.get()){
            Optional<ProductModel> productsAmount = productService.findById(hM.getIdProduct());
            amount += productsAmount.isPresent() ? productsAmount.get().getPrice() : 0;
        }

        if((amount *  .30) >= enterpriseResult.get().getMonthlyIncome()){
            System.out.println("La empresa no puede hacer mas contrataciones de este producto");
            result.setMsg("La empresa no puede contratar por superar sus ingresos mensuales");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if(hiringResult.isPresent() && productResult.get().getHiringLimit() <= hiringResult.get().size()){
            System.out.println("La empresa no puede contratar por exceso de contrataciones del mismo producto");
            result.setMsg("La empresa no puede contratar por exceso de contrataciones del mismo producto");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }


        this.hiringRepository.save(hiring);
        result.setMsg("Contratacion exitosa");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }
}
