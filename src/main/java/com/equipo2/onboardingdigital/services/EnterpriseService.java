package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.EnterpriseModel;
import com.equipo2.onboardingdigital.repositories.EnterpriseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EnterpriseService {

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    public List<EnterpriseModel> findAll() {
        return enterpriseRepository.findAll();
    }

    public Optional<EnterpriseModel> findById(String id) {
        return enterpriseRepository.findById(id);
    }

    public EnterpriseModel save(EnterpriseModel productModel) {
        return enterpriseRepository.save(productModel);
    }

}
