package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.CustomersModel;
import com.equipo2.onboardingdigital.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    public List<CustomersModel> findAll() {
        System.out.println(" ::findAll Customers:: ");
        List<CustomersModel> customer = this.customerRepository.findAll();
        Collections.sort(customer, new Comparator<CustomersModel>() {
            @Override
            public int compare(CustomersModel p1, CustomersModel p2) {
                return new Integer(p2.getId()).compareTo(new Integer(p1.getId()));
            }
        });

        return customer;
    }

    public Optional<CustomersModel> getById(String id) {
        System.out.println(" ::getById Customer:: ");
        return this.customerRepository.findById(id);
    }

    public CustomerServiceResponse add(CustomersModel customer) {
        System.out.println(" ::add Customer:: ");
        CustomerServiceResponse response = new CustomerServiceResponse();
            List<CustomersModel> list = this.customerRepository.findAll();
            Date date = new Date();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String da = df.format(date);
            customer.setHighDate(da);
            if (list.size() > 0) {
                CustomersModel obj = list.get(list.size() - 1);
                int idc = Integer.parseInt(obj.getId()) + 1;
                String id = String.valueOf(idc);
                customer.setId(id);
            } else {
                customer.setId("1");
            }
            this.customerRepository.save(customer);
            response.setCustomer(customer);
            response.setMsg("Cliente creado");
            response.setResponseHttpStatusCode(HttpStatus.CREATED);

        return response;
    }
}
