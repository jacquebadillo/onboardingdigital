package com.equipo2.onboardingdigital.services;

import com.equipo2.onboardingdigital.models.ProductModel;
import com.equipo2.onboardingdigital.repositories.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll en productService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en productService");

        return this.productRepository.findById(id);
    }

    public ProductServiceResponse add(ProductModel product) {
        System.out.println("add en productService");

        ProductServiceResponse response = new ProductServiceResponse();

        response.setProductModel(product);

        if (this.findById(product.getId()).isPresent() == true){
            System.out.println("El id del producto a guardar ya existe");
            response.setMsg("El id del producto a guardar ya existe");
            response.setResponseHttpStatus(HttpStatus.BAD_REQUEST);

            return response;
        }
        if(product.getAcceptanceMin() > product.getAcceptanceMax()){
            System.out.println("El AcceptanceMin es mayor al getAcceptanceMax ");
            response.setMsg("El AcceptanceMin es mayor al getAcceptanceMax");
            response.setResponseHttpStatus(HttpStatus.BAD_REQUEST);

            return response;
        }

        this.productRepository.save(product);
        response.setMsg("Producto añadido correctamente");
        response.setResponseHttpStatus(HttpStatus.CREATED);

        return response;
    }
}
