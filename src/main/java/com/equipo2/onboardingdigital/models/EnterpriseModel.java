package com.equipo2.onboardingdigital.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document(collection = "enterprises")
public class EnterpriseModel {

    @Id
    private String id;
    private String idCustomer;
    private String companyName;
    private String rfc;
    private float monthlyIncome;
    private AddressModel address;
    private String entryDate;
    private String leavingDate;

    public EnterpriseModel() {
    }

    public EnterpriseModel(String id, String idCustomer, String companyName, String rfc, float monthlyIncome, AddressModel address, String entryDate, String leavingDate) {
        this.id = id;
        this.idCustomer = idCustomer;
        this.companyName = companyName;
        this.rfc = rfc;
        this.monthlyIncome = monthlyIncome;
        this.address = address;
        this.entryDate = entryDate;
        this.leavingDate = leavingDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public float getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(float monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getLeavingDate() {
        return leavingDate;
    }

    public void setLeavingDate(String leavingDate) {
        this.leavingDate = leavingDate;
    }
}
