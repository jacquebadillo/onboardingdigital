package com.equipo2.onboardingdigital.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "hirings")
public class HiringModel {

    @Id
    private String id;
    private String idEnterprise;
    private String idProduct;
    private String hiringDate;
    private String family;

    public HiringModel() {
    }

    public HiringModel(String id, String idEnterprise, String idProduct, String hiringDate, String family) {
        this.id = id;
        this.idEnterprise = idEnterprise;
        this.idProduct = idProduct;
        this.hiringDate = hiringDate;
        this.family = family;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEnterprise() {
        return idEnterprise;
    }

    public void setIdEnterprise(String idEnterprise) {
        this.idEnterprise = idEnterprise;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(String hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }
}
