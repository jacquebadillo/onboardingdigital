package com.equipo2.onboardingdigital.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products")
public class ProductModel {

    @Id
    private String id;
    private String name;
    private String description;
    private float acceptanceMax;
    private float acceptanceMin;
    private String family;
    private float hiringLimit;
    private float price;

    public ProductModel() {
    }

    public ProductModel(String id, String name, String description, float acceptanceMax, float acceptanceMin, String family, float hiringLimit, float price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.acceptanceMax = acceptanceMax;
        this.acceptanceMin = acceptanceMin;
        this.family = family;
        this.hiringLimit = hiringLimit;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getAcceptanceMax() {
        return acceptanceMax;
    }

    public void setAcceptanceMax(float acceptanceMax) {
        this.acceptanceMax = acceptanceMax;
    }

    public float getAcceptanceMin() {
        return acceptanceMin;
    }

    public void setAcceptanceMin(float acceptanceMin) {
        this.acceptanceMin = acceptanceMin;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public float getHiringLimit() {
        return hiringLimit;
    }

    public void setHiringLimit(float hiringLimit) {
        this.hiringLimit = hiringLimit;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
