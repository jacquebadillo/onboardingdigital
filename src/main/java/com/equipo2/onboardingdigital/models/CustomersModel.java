package com.equipo2.onboardingdigital.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customers")
public class CustomersModel {
    @Id
    private String id;
    private String name;
    private int age;
    private String idDocument;
    private String highDate;
    private String lowDate;

    public CustomersModel() {
    }

    public CustomersModel(String id, String name, int age, String idDocument, String highDate, String lowDate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.idDocument = idDocument;
        this.highDate = highDate;
        this.lowDate = lowDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(String idDocument) {
        this.idDocument = idDocument;
    }

    public String getHighDate() {
        return highDate;
    }

    public void setHighDate(String highDate) {
        this.highDate = highDate;
    }

    public String getLowDate() {
        return lowDate;
    }

    public void setLowDate(String lowDate) {
        this.lowDate = lowDate;
    }
}